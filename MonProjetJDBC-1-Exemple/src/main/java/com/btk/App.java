package com.btk;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
	public static void main(String[] args) {
		try {
			
			/***
			 *  GIt Eclipse
			 *  salah Ben Alaya
			 *  
			 *  
			 */
			
			
			// Chargement Driver

			Class.forName("com.mysql.jdbc.Driver");

			// Connexion sur la BD

			String url = "jdbc:mysql://localhost:3306/bankdb";
			String user = "root";
			String password = "password";
			Connection conn = DriverManager.getConnection(url, user, password);

			// Formulation de la requête

			Statement statement = conn.createStatement();

			// Exécution de la requête

			ResultSet resultSet = statement.executeQuery("select * from comptes");

			// Exploitation des résultats

			while (resultSet.next()) {
				String num = resultSet.getString("numero");
				String prop = resultSet.getString("proprietaire");
				BigDecimal solde = resultSet.getBigDecimal("solde");
				System.out.println("Compte : " + num + " - " + prop + " - " + solde);
			}

			// Fermeture de la connexion
			conn.close();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
