package com.btk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.math.BigDecimal;

import org.hamcrest.core.SubstringMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.btk.business.Compte;

public class CompteTest {

	private Compte underTest;

	@Before
	public void setUp() throws Exception {
		underTest = new Compte("C1000", "Saadaoui Nizar", new BigDecimal("1000"));

	}

	@After
	public void tearDown() throws Exception {
		underTest = null;
	}

	@Test
	public final void testCrediter() {
		// fail("Not yet implemented"); // TODO

		// ARRANGE
		BigDecimal somme = new BigDecimal("450");
		BigDecimal expected = new BigDecimal("1450");
		// ACT
		underTest.crediter(somme);
		BigDecimal actual = underTest.getSolde();

		// ASSERT
		assertEquals(expected, actual);
	}

	@Test
	public final void testDebiter() {
		//fail("Not yet implemented"); // TODO
		
		// ARRANGE
		BigDecimal somme = new BigDecimal("450");
		BigDecimal expected = new BigDecimal("550");
		// ACT
		underTest.debiter(somme);
		BigDecimal actual = underTest.getSolde();

		// ASSERT
		assertEquals(expected, actual);
	}

}
