package com.btk.business;

import java.math.BigDecimal;

public class Compte {

	// Attributs privés
	private String numero;
	private String proprietaire;
	private BigDecimal solde;

	// Constructeur par défaut
	public Compte() {
		super();
	}

	// Constructeur avec argument
	public Compte(String numero, String proprietaire, BigDecimal solde) {
		super();
		this.numero = numero;
		this.proprietaire = proprietaire;
		this.solde = solde;
	}

	// Setters & Getters
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}

	public BigDecimal getSolde() {
		return solde;
	}

	public void setSolde(BigDecimal solde) {
		this.solde = solde;
	}
	
	
	public void crediter (BigDecimal somme) {
		this.solde = this.solde.add(somme);
	}
	
	
	public void debiter (BigDecimal somme) {
		this.solde = this.solde.subtract(somme);
		
	}
	
	

}
